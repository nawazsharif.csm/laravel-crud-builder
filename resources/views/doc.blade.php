<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dynamic add/remove input fields in PHP Jquery AJAX</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<div class="card text-center" style="margin-bottom:50px;">
  <h1>Laravel Crud code generator</h1>
</div>

<div class="container">
  <div class="row">
      <h2 style="width:100%">Migration</h2>
      <h4 class="w-100">{{$migration_command}}</h4>
      <?prettify linenums=1?>
      <pre class="prettyprint lang-php">
        {{$migration}}
      </pre>
      <h2 style="width:100%">Model</h2>
      <h4>{{$modelCommand}}</h4>
      <br>
      <br>
      <br>
      <h2 style="width:100%">Router</h2>
      <?prettify linenums=1?>
        <pre class="prettyprint lang-php">
            {{$router}}
        </pre>
      <br>
      <br>
      <br>

      <h2 style="width:100%">Controller</h2>
      <h4>{{$controller_command}}</h4>
      <?prettify linenums=1?>
        <pre class="prettyprint lang-php">
            {{$controller}}
        </pre>

    <h2 style="width:100%">List View</h2>
      <p class="w-100">Put this code to show list</p>
      <?prettify linenums=1?>
      <pre class="prettyprint lang-html">

        {{$ListView}}
    </pre>
    <h2 style="width:100%">Create Update View</h2>
      <?prettify linenums=1?>
    <pre class="prettyprint lang-html">
        {{$create_edit_view}}
    </pre>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{asset('https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?skin=sons-of-obsidian')}}"></script>
</body>
</html>
