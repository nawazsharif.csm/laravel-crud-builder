<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo Inflector::pluralize('search');
        // dd();
        $data = request()->all();
        $migration = '';
        $data['migration_command'] = 'php artisan make:migration create_' . strtolower($data['model']) . 's_table';
        // dd(count($data['name']));
        $migration .= '$table->id();';
        for ($i = 0; $i < count($data['name']); $i++) {
            if (!empty($data['validetion'][$i])) {
                $val = '';
            } else {
                $val = '->nullable()';
            }
            $migration .= '
        $table->' . $data['type'][$i] . '(\'' . $data['name'][$i] . '\')' . $val . ';';
        }
        $del = '';
        if ($data['softdelete']) {
            $migration .= '
        $table->softDeletes($column = \'deleted_at\', $precision = 0);';
            $del = 'where([\'soft_delete\'=>0])';
        }
        $migration .= '
        $table->timestamps();';
        $data['migration'] = $migration;

        $data['modelCommand'] = 'php artisam make:model ' . $data['model'] . ' -m';

        $data['controller_command'] = 'php artisan make:Controller ' . $data['controller'];

        $data['router'] = '
        Route::group([\'prefix\' => \'' . strtolower($data['model']) . '\'], function () {
            Route::get(\'/\', \'' . $data['controller'] . '@index\')->name(\'' . strtolower($data['model']) . '.index\');
            Route::get(\'create\', \'' . $data['controller'] . '@create\')->name(\'' . strtolower($data['model']) . '.create\');
            Route::post(\'/create\', \'' . $data['controller'] . '@postCreate\')->name(\'' . strtolower($data['model']) . '.create\');
            Route::get(\'/edit/{id}\', \'' . $data['controller'] . '@edit\')->name(\'' . strtolower($data['model']) . '.edit\');
            Route::post(\'/edit/{id}\', \'' . $data['controller'] . '@postEdit\')->name(\'admin_news.edit\');
            Route::post(\'/delete\', \'' . $data['controller'] . '@deleteList\')->name(\'' . strtolower($data['model']) . '.delete\');
        });
        ';

        $controller = '';
        $controller .= '
        public function index()
        {
            ';

        $controller .= '$data[\'' . strtolower($data['model']) . '\']=  ' . $data['model'] . '::' . $del . '->get();';

        $controller .= '
            $data = [
                \'title\'         => \'put your title\',
                \'subTitle\'      => \'\',
                \'icon\'          => \'fa fa-indent\',
                \'urlDeleteItem\' => route(\'' . strtolower($data['model']) . '.delete\'),
                \'css\'           => \'\',
                \'js\'            => \'\',
            ];';

        $controller .= '
            $listTh = [
                ';
        $controller .= '\'id\'     => \'id\',';
        for ($i = 0; $i < count($data['name']); $i++) {
            $controller .= '
                \'' . $data['name'][$i] . '\'=> \'' . ucfirst($data['name'][$i]) . '\',';
        }

        $controller .= '
                \'action\' => \'Action\',';
        $controller .= '
            ];';

        $controller .= '
            $obj = new ' . $data['model'] . '();';
        if ($data['softdelete']) {
            $controller .= '$obj = $obj->where(\'deleted_at\',0)->orderBy(\'id\', \'desc\');';
        } else {
            $controller .= '$obj = $obj->orderBy(\'id\', \'desc\');';
        }

        $controller .= '
            $dataTmp = $obj->paginate(20);';

        $controller .= '
            $dataTr = [];
            ';
        $controller .= '
            foreach ($dataTmp as $key => $row) {
                $dataTr[] = [';
        $controller .= '
                        \'id\' => $row[\'id\'],';

        for ($i = 0; $i < count($data['name']); $i++) {
            $controller .= '
                        \'' . $data['name'][$i] . '\'=> $row[\'' . $data['name'][$i] . '\'],';
        }
        $controller .= '
                        \'action\' => \'<a href="\' . route(\'' . strtolower($data['model']) . 'edit\', [\'id\' => $row[\'id\']] ). \'"><span title="Edit" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;<span onclick="deleteItem(\' . $row[\'id\'] . \');"  title="Delete" class="btn btn-flat btn-danger"><i class="fas fa-trash-alt"></i></span>\'
                ];';
        $controller .= '
            }';

        $controller .= '
            $data[\'listTh\'] = $listTh;';
        $controller .= '
            $data[\'dataTr\'] = $dataTr;';

        // $controller .= '$data[\'resultItems\'] = \'Total\', [\'item_from\' => $dataTmp->firstItem(), \'item_to\' => $dataTmp->lastItem(), \'item_total\' => $dataTmp->total()]);';


        $controller .= '
            return view(\'screen.list\')->with($data);';

        $controller .= '
        }
        public function create(){

            $data = [

                \'title\' => \'Put Your Title\',
                \'subTitle\' => \'\',
                \'title_description\' => \'Title Description\',
                \'icon\' => \'fa fa-plus\',
                \'url_action\' => route(\'' . strtolower($data['model']) . '.create\'),
            ];

            return view(\'index\')
                ->with($data);


        }
        public function store(Request $request)
        {

            $data = request()->all();
            $validator = Validator::make($data, [';

        for ($i = 0; $i < count($data['name']); $i++) {
            if ($data['validetion'][$i]) {
                $controller .= '
                    \'' . $data['name'][$i] . '\' => \'' . $data['validetion'][$i] . '\',';
            }
        }
        $controller .= '
            ]);


            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($data);
            }
            $dataInsert = [';
        for ($i = 0; $i < count($data['name']); $i++) {
            $controller .= '
                \'' . $data['name'][$i] . '\' => $data[\'' . $data['name'][$i] . '\'],';
        }
        $controller .= '
            ];

            $obj = ' . $data['model'] . '::insert($dataInsert);
            notify()->success(\'Message Put here\');
            return redirect()->route(\'' . strtolower($data['model']) . 'list\')->with(\'success\', \'Success message put here\');

        }
        public function show($id){

            $' . strtolower($data['model']) . ' = ' . $data['model'] . '::find($id);
            $data = [
                \'title\' => \'Edit Title\',
                \'title_action\' => \'<i class="fa fa-edit" aria-hidden="true"></i> Edit \',
                \'subTitle\' => \'\',
                \'icon\' => \'fa fa-indent\',
                \'urlDeleteItem\' => route(\'' . strtolower($data['model']) . 'delete\'),
                \'css\' => \'\',
                \'js\' => \'\',
                \'url_action\' => route(\'' . strtolower($data['model']) . 'edit\', [\'id\' => $info[\'id\']]),
                \'' . strtolower($data['model']) . '\' => $' . strtolower($data['model']) . ',
                \'id\' => $id,
            ];

            $data[\'layout\'] = \'edit\';
            return view(\'' . strtolower($data['model']) . 'index\')
                ->with($data);

        }
        public function edit($id){
            $' . strtolower($data['model']) . ' = ' . $data['model'] . '::find($id);
            $data = [
                \'title\' => \'Edit Title\',
                \'title_action\' => \'<i class="fa fa-edit" aria-hidden="true"></i> Edit \',
                \'subTitle\' => \'\',
                \'icon\' => \'fa fa-indent\',
                \'urlDeleteItem\' => route(\'' . strtolower($data['model']) . 'delete\'),
                \'css\' => \'\',
                \'js\' => \'\',
                \'url_action\' => route(\'' . strtolower($data['model']) . 'edit\', [\'id\' => $info[\'id\']]),
                \'' . strtolower($data['model']) . '\' => $' . strtolower($data['model']) . ',
                \'id\' => $id,
            ];

            $data[\'layout\'] = \'edit\';
            return view(\'' . strtolower($data['model']) . 'index\')
                ->with($data);
        }
        public function update(Request $request, $id){
            $data = request()->all();
            $validator = Validator::make($data, [';

        for ($i = 0; $i < count($data['name']); $i++) {
            if ($data['validetion'][$i]) {
                $controller .= '\'' . $data['name'][$i] . '\' => \'' . $data['validetion'][$i] . '\',';
            }
        }
        $controller .= ']);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput($data);
            }
            $dataInsert = [';
        for ($i = 0; $i < count($data['name']); $i++) {
            $controller .= '
                \'' . $data['name'][$i] . '\' => $data[\'' . $data['name'][$i] . '\'],';
        }
        $controller .= '
            ];

            $obj = ' . $data['model'] . '::where([\'id\'=>$id])->update($dataInsert);
            notify()->success(\'Message Put here\');
            return redirect()->route(\'' . strtolower($data['model']) . 'list\')->with(\'success\', \'Success message put here\');
        }
        public function destroy($id){
            if (!request()->ajax()) {
                return response()->json([\'error\' => 1, \'msg\' => \'You cant delete\']);
            } else {
                $ids = request(\'ids\');
                $arrID = explode(\',\', $ids);
                ' . $data['model'] . '::where(\'id\', $arrID)->update([
                    \'deleted_at\' => Carbon::now()
                ]);
                notify()->success(\'Delete Succesfully\');
                return response()->json([\'error\' => 0, \'msg\' => \'\']);
            }
        }

    ';

        $data['controller'] = $controller;

        $data['ListView'] = '
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                @if (!empty($topMenuRight) && count($topMenuRight))
                @foreach ($topMenuRight as $item)
                <div class="menu-right">
                    @php
                    $arrCheck = explode(\'view::\', $item);
                    @endphp
                    @if (count($arrCheck) == 2)
                    @if (view()->exists($arrCheck[1]))
                    @include($arrCheck[1])
                    @endif
                    @else
                    {!! trim($item) !!}
                    @endif
                </div>
                @endforeach
                @endif
                </div>
                <div class="float-left">
                @if (!empty($topMenuLeft) && count($topMenuLeft))
                @foreach ($topMenuLeft as $item)
                <div class="menu-left">
                    @php
                    $arrCheck = explode(\'view::\', $item);
                    @endphp
                    @if (count($arrCheck) == 2)
                    @if (view()->exists($arrCheck[1]))
                    @include($arrCheck[1])
                    @endif
                    @else
                    {!! trim($item) !!}
                    @endif
                </div>
                @endforeach
                @endif
                </div>

                <div class="card-tools">
                @if (!empty($menuRight) && count($menuRight))
                @foreach ($menuRight as $item)
                <div class="menu-right">
                    @php
                    $arrCheck = explode(\'view::\', $item);
                    @endphp
                    @if (count($arrCheck) == 2)
                    @if (view()->exists($arrCheck[1]))
                    @include($arrCheck[1])
                    @endif
                    @else
                    {!! trim($item) !!}
                    @endif
                </div>
                @endforeach
                @endif
                </div>


                <div class="float-left">
                @if (!empty($menuLeft) && count($menuLeft))
                @foreach ($menuLeft as $item)
                <div class="menu-left">
                    @php
                    $arrCheck = explode(\'view::\', $item);
                    @endphp
                    @if (count($arrCheck) == 2)
                    @if (view()->exists($arrCheck[1]))
                    @include($arrCheck[1])
                    @endif
                    @else
                    {!! trim($item) !!}
                    @endif
                </div>
                @endforeach
                @endif

                </div>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                    @if (!empty($removeList))
                    <th></th>
                    @endif
                    @foreach ($listTh as $key => $th)
                    <th>{!! $th !!}</th>
                    @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataTr as $keyRow => $tr)
                    <tr>
                    @if (!empty($removeList))
                    <td>
                        <input class="checkbox grid-row-checkbox" type="checkbox" data-id="{{ $tr[\'id\']??\'\' }}">
                    </td>
                    @endif
                    @foreach ($tr as $key => $trtd)
                    <td>{!! $trtd !!}</td>
                    @endforeach
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>';

        $create_edit_view = '';
        $create_edit_view .= '

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header with-border">
					<h2 class="card-title">{{ $title_description??\'\' }}</h2>

					<div class="card-tools">
						<div class="btn-group float-right mr-5">
							<a href="{{ route(\'' . strtolower($data['model']) . '.list\') }}" class="btn  btn-flat btn-default" title="List"><i
										class="fa fa-list"></i><span
										class="hidden-xs"> Back</span></a>
						</div>
					</div>
				</div>
				<form action="{{ $url_action }}" method="post" accept-charset="UTF-8" class="form-horizontal" id="form-main" enctype="multipart/form-data">
					<div class="card-body">
						<div class="fields-group">';
        for ($i = 0; $i < count($data['name']); $i++) {
            $create_edit_view .= '
                                                        <div class="form-group  row">
								<div class="col-md-6">
									<div class="col-md-4">
										<label for="name" class="control-label">Name (*)</label>
									</div>
									<div class="col-md-12">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-pencil-alt"></i></span>
											</div>
											<input type="text" id="name" name="name" value="{{ old(\'' . $data['name'][$i] . '\',$' . strtolower($data['model']) . '[\'' . $data['name'][$i] . '\']??\'\')}}" class="form-control name" placeholder=""/>
										</div>
										@if ($errors->has(\'' . $data['name'][$i] . '\'))
											<span class="form-text"> <i class="fa fa-info-circle"></i> {{ $errors->first(\'' . $data['name'][$i] . '\') }}</span>
										@endif
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>';
        }

        $create_edit_view .= '
                        </div>
					</div>
					<div class="card-footer">
						@csrf
						<div class="col-md-2">
						</div>
						<div class="col-md-8">
							<div class="btn-group float-right">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
';
        $data['create_edit_view'] = $create_edit_view;
        return view('doc', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
